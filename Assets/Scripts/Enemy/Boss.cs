using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Boss : MonoBehaviour, IDamageable
{

    [Header("Variable")]
    [SerializeField] int health;
    [SerializeField] int damage;
    [SerializeField] float timeToShoot;
    [SerializeField] float cursorSpeed;
    [Header("Reference")]
    [SerializeField] private Transform followPlayer;
    [SerializeField] private Transform player;

    private bool shoot = true;
    private SpawnerPool pool;


    private void Awake()
    {
        pool = GetComponent<SpawnerPool>();
    }


    private void Update()
    {
        if (gameObject.activeSelf)
        {
            followPlayer.parent = null;
        }

        EnemyShoot();
        MoveEnemy();
    }
   
    protected void MoveEnemy()
    {
        followPlayer.position = Vector3.MoveTowards(followPlayer.transform.position, player.transform.position, cursorSpeed * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(followPlayer.position.y, -2.6f, 2.6f), transform.position.z);
    }
    private void EnemyShoot()
    {
        if (shoot)
        {
            pool.SpawnBoss();
            StartCoroutine(dalay());
        }
    }

    private IEnumerator dalay()
    {
        shoot = false;
        yield return new WaitForSeconds(timeToShoot);
        shoot = true;

    }

    private void OnCollisionEnter(Collision collision)
    {
        IDamageable damageable = collision.collider.GetComponent<IDamageable>();
        damageable?.TakeDamage(damage);

    }

    public void TakeDamage(int damage)
    {
        var tmp = SceneManager.GetActiveScene().buildIndex;
        Debug.Log(tmp);


        health -= damage;

        if (health < 0)
        {
            if (tmp != 3)
            {
                tmp++;
                SceneManager.LoadScene(tmp);
            }
            else
                SceneManager.LoadScene("Victory");

            gameObject.SetActive(false);
        }
    }
}
