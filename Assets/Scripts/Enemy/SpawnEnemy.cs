using System.Collections;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{

    private Transform spawnTop;
    private Transform spawnDown;

    private bool SetTimer = true;
    private float timerEnemy;

    [SerializeField] private GameObject enemyObj;


    [Header("Variable")]
    [SerializeField, Tooltip("Min time to spawn enemy")] float minTime;
    [SerializeField, Tooltip("Max time to spawn enemy")] float maxTime;

    private void Awake()
    {
        spawnTop = transform.GetChild(0);
        spawnDown = transform.GetChild(1);
    }
    private void Update()
    {
        if (SetTimer)
        {
            timerEnemy = Random.Range(minTime, maxTime);
            StartCoroutine(enemySpawn());
        }
    }

    public IEnumerator enemySpawn()
    {
        SetTimer = false;
        yield return new WaitForSeconds(timerEnemy);
        CreateEnemy();
        SetTimer = true;
    }

    private void CreateEnemy()
    {
        // spawn
        float positionY = Random.Range(spawnTop.transform.position.y, spawnDown.transform.position.y);
        Vector3 spawn = new Vector3(spawnTop.position.x, positionY, 0);
        GameObject spawnEn = Instantiate(enemyObj, spawn, enemyObj.transform.rotation);
        Destroy(spawnEn, 12f);
    }

}
