using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTank :Enemy
{
    protected override void Start()
    {
        base.Start();
    }
    private void Update()
    {
        Obstacle(Vector3.up, Vector3.down,1.5f);
        Obstacle(Vector3.down, Vector3.up,1.5f);
        base.EnemyShoot();
    }
    private void FixedUpdate()
    {
        base.MoveEnemy();
    }
}
