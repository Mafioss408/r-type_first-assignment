using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour, IDamageable
{
    [Header("Variable")]

    [SerializeField] protected int health;
    [SerializeField] protected int damage;
    [SerializeField] protected float speed;
    [SerializeField] protected float timeToShoot;
    [Header("Reference")]
    [SerializeField] protected Transform muzzleMove;
    [SerializeField] protected Transform player;
    [SerializeField] protected LayerMask wallMask;

    private bool shoot = true;
    private SpawnerPool pool;


    private void Awake()
    {
        pool = GetComponent<SpawnerPool>();
    }

    protected virtual void Start()
    {
        player = FindObjectOfType<PlayerController>().transform;
    }

    protected void MoveEnemy()
    {
        transform.Translate(-Vector3.right * speed * Time.deltaTime);
    }

    protected void EnemyShoot()
    {
        muzzleMove.LookAt(player.transform.position);

        if (shoot)
        {
            pool.SpawnEnemy();
            StartCoroutine(dalay());
        }

    }

    private IEnumerator dalay()
    {
        shoot = false;
        yield return new WaitForSeconds(timeToShoot);
        shoot = true;

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Wall"))
        {
            IDamageable damageable = collision.collider.GetComponent<IDamageable>();
            damageable?.TakeDamage(damage);

        }

    }

    protected void Obstacle(Vector3 myPos, Vector3 myDirection, float distanceRay)
    {
        RaycastHit hit;
        Physics.Raycast(transform.position, myPos, out hit, distanceRay, wallMask);
        Debug.DrawRay(transform.position, transform.up, Color.red, 2f);

        if (hit.collider == null) return;
        if (hit.collider != null)
        {
            transform.position += myDirection * speed * Time.deltaTime;
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }
}
