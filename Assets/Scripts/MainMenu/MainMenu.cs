using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;


public class MainMenu : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textTimer;
    [SerializeField] private float timer;


    private void Update()
    {
        TimerMenu();
    }

    private void TimerMenu()
    {
        timer -= Time.deltaTime;
        textTimer.text = timer.ToString("0");

        if (timer <= 0)
            SceneManager.LoadScene(0);
    }

}
