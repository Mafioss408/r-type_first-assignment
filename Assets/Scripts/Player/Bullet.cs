using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class Bullet : MonoBehaviour
{

    Rigidbody rb;

    [SerializeField] private float bulletSpeed;
    [SerializeField] private int bulletDamage;

    public int _bulletDamage { get { return bulletDamage; } private set { bulletDamage = value; } }

  
    private void Update()
    {
        moveBullet();
        StartCoroutine(DestroyObj());
    }

    private void moveBullet()
    { 
        transform.Translate(Vector3.right * bulletSpeed * Time.deltaTime);
    }

    private IEnumerator DestroyObj()
    {
        yield return new WaitForSeconds(3f);
        ProjectilePool.Instance.ReturnToPool(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {

        IDamageable damageable = collision.collider.GetComponent<IDamageable>();
        damageable?.TakeDamage(bulletDamage);

        ProjectilePool.Instance.ReturnToPool(gameObject);
    }
}
