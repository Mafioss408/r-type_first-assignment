using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour, IDamageable
{

    private Rigidbody rb;
    private ParticleSystem ps;
    private Camera camera;
    private SpawnerPool pool;
    private GameManager _manager;
    private float actualSpeed;
    private bool isDeath = true;


    [Header("Variable")]
    [SerializeField] private float health = 100f;
    [SerializeField] private float speed = 5;
    [SerializeField] private int damage = 5;


    [Header("Reference")]
    [SerializeField] private Scene scene;
    [SerializeField] LayerMask enemy;

    public float _speed { get { return speed; } set { speed = value; } }
    public float _playerHealth { get { return health; } set { health = value; } }


    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        pool = GetComponent<SpawnerPool>();
        ps = GetComponent<ParticleSystem>();
        camera = Camera.main;

    }

    private void Start()
    {
        _manager = GameManager.Instance;
        ps.Stop();
        actualSpeed = speed;
    }


    void FixedUpdate() => Move();


    private void Update()
    {
        if (Input.GetButtonDown("Jump") && _manager.statusGame == GameManager.GameStatus.RunningGame)
        {
            Audio.OnAudio?.Invoke("Shoot");
            pool.Spawn();
        }
    }


    private void Move()
    {

        float limitixMin = camera.transform.position.x - 7.9f;
        float limitixMax = camera.transform.position.x + 7.9f;

        Vector2 move = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        rb.velocity = new Vector2(move.x, move.y).normalized * speed * Time.deltaTime;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, limitixMin, limitixMax), Mathf.Clamp(transform.position.y, -3.75f, +3.75f), 0);

    }


    public void TakeDamage(int damage)
    {
        if (health > 0)
        {
            health -= damage;

        }

        if (health <= 0)
        {
            if (_manager._indexLife > 1)
            {
                if (isDeath)
                    StartCoroutine(Die());
            }
            else
                StartCoroutine(Loose());
        }
    }

    private IEnumerator Die()
    {
        isDeath = false;
        Audio.OnAudio?.Invoke("Boom");
        int var = SceneManager.GetActiveScene().buildIndex;
        ps.Play();
        speed = 0;
        yield return new WaitForSeconds(1);
        scene._Progress = scene._MaxTimeProgress;
        _manager._indexLife--;
        speed = actualSpeed;
        isDeath = true;
        SceneManager.LoadScene(var);

    }

    private IEnumerator Loose()
    {
        isDeath = false;
        Audio.OnAudio?.Invoke("Boom");
        ps.Play();
        speed = 0;
        yield return new WaitForSeconds(1);
        scene._Progress = scene._MaxTimeProgress;
        isDeath = true;
        SceneManager.LoadScene("Loose");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.CompareTag("Boss"))
        {
            IDamageable damageable = collision.collider.GetComponent<IDamageable>();
            damageable?.TakeDamage(damage);

        }

    }
}

