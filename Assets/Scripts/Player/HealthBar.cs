using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    [Header("Reference")]
    [SerializeField] private PlayerController player;

    private Slider slider;


    private void Awake()
    {
        slider = GetComponent<Slider>();

    }
    void Start()
    {

        slider.maxValue = player._playerHealth;
    }


    void Update()
    {
        slider.value = player._playerHealth;
    }



}
