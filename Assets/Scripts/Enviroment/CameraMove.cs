using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    private Camera camera;

    [Header("Variable")]
    [SerializeField] private float speedCamera = 5;

    public float _speedCamera { get { return speedCamera; } set { speedCamera = value; } }

    private void Awake()
    {
        camera = Camera.main;

    }
    private void Update() =>  transform.Translate(Vector3.right * speedCamera * Time.deltaTime);

}
