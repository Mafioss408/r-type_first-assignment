using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    [Header("Reference")]
    [SerializeField] Transform camera;
    [SerializeField] private Transform[] background;
    [SerializeField] private int sizeBackground;

    private int leftIndex, rightIndex;

    void Start()
    {

        camera = Camera.main.transform;
        background = new Transform[transform.childCount];


        for (int i = 0; i < transform.childCount; i++)
        {
            background[i] = transform.GetChild(i);
        }

        leftIndex = 0;
        rightIndex = background.Length - 1;
    }

    
    void Update()
    {
        if (camera.transform.position.x > (background[rightIndex].transform.position.x))
        {

            background[leftIndex].position = new Vector3(background[rightIndex].position.x + sizeBackground, 0, 2);

            rightIndex = leftIndex;
            leftIndex++;

            if (leftIndex == background.Length)
            {
                leftIndex = 0;
            } 
        }
    }
}

