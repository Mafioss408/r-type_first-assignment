using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{

    [Header("Reference")]
    [SerializeField] GameObject obstacle;



    private Transform spawnTop;
    private Transform spawnDown;

    private Vector3 changeScale;
  
    private bool _spawnTop = true;
    private bool _spawnDown = true;

    private void Awake()
    {
        spawnTop = transform.GetChild(0);
        spawnDown = transform.GetChild(1);

    }

    private void FixedUpdate()
    {
        float randomTimer = Random.Range(2f, 8f);

        if (_spawnTop)
            StartCoroutine(timer( randomTimer, spawnTop));
        if (_spawnDown)
            StartCoroutine(timerDown( randomTimer, spawnDown));
    }


    private void SpawnObstacle(Transform position)
    {
        float x = Random.Range(4, 10);
        float y = Random.Range(1, 2.5f);

        changeScale = new Vector3(x, y, 1);
        obstacle.transform.localScale = changeScale;

        GameObject spawnUp = Instantiate(obstacle, position.transform.position, Quaternion.identity);
    }

    private IEnumerator timer(float timer, Transform pos)
    {
        _spawnTop = false;
        yield return new WaitForSeconds(timer);
        SpawnObstacle(pos);
        _spawnTop = true;
    }
    private IEnumerator timerDown(float timer, Transform pos)
    {
        _spawnDown = false;
        yield return new WaitForSeconds(timer);
        SpawnObstacle(pos);
        _spawnDown = true;
    }

}
