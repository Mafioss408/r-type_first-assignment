using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    private GameManager gameManager;

    private void Start()
    {
        gameManager = GameManager.Instance;
    }
    public void StartGame(int value)
    {
        gameManager._indexLife = 3;
        SceneManager.LoadScene(value);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
