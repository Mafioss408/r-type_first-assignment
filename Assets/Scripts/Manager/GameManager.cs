using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Variable")]
   
    [SerializeField] private int indexLife = 3;

    public int _indexLife { get { return indexLife; } set { indexLife = value; } }
   
    public enum GameStatus
    {
        RunningGame,
        PauseGame,
    }

    [SerializeField] public GameStatus statusGame;


    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
        private set { instance = value; }
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;

        }
    }
}

