using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour
{
    private Camera camera;
    private CameraMove camMove;
    private GameManager _manager;
    private Vector3 bossActualPosition;
    private bool setBoss = false;
  

    [Header("Reference")]
    [SerializeField] private List<GameObject> _objects = new List<GameObject>();
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject boss;
    [SerializeField] private TextMeshProUGUI textLife;
    [SerializeField] private Transform bossPosition;
    [Header("Variable")]
    [SerializeField] private float speedBoss;
    [SerializeField] private float timerSpawn;
    [SerializeField] private float timeProgress;
    [SerializeField] private float maxtimeProgress;

    public float _Progress { get { return timeProgress; } set { timeProgress = value; } }
    public float _MaxTimeProgress { get { return maxtimeProgress; } set { maxtimeProgress = value; } }

    private void Awake()
    {
        camera = Camera.main;
    }
    private void Start()
    {
        camMove = camera.GetComponent<CameraMove>();
        _manager = GameManager.Instance;
        SetText();
    }
    private void Update()
    {
        bossActualPosition = boss.transform.position;
        TimerLevel();
        PauseMenu();
        BossStage();

        if (setBoss)
        {
            boss.transform.position = Vector3.MoveTowards(bossActualPosition, bossPosition.transform.position,speedBoss *  Time.deltaTime);
            if(Vector3.Distance(boss.transform.position ,bossPosition.transform.position) < 0.02f)
            {
                setBoss=false;  
            }

        }
    }

    private void PauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && _manager.statusGame == GameManager.GameStatus.RunningGame)
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0f;
            _manager.statusGame = GameManager.GameStatus.PauseGame;
        }
    }
    private void BossStage()
    {
        if (timeProgress <= 0f)
        {
            foreach (var item in _objects)
            {
                item.SetActive(false);
            }
            camMove._speedCamera = 0;
            StartCoroutine(dalay());
        }
    }

    private IEnumerator dalay()
    {
        yield return new WaitForSeconds(timerSpawn);
        boss.SetActive(true);
        setBoss = true;
    }

    public void GameRunning()
    {
        if (_manager.statusGame == GameManager.GameStatus.PauseGame)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1f;
            _manager.statusGame = GameManager.GameStatus.RunningGame;
        }
    }
    public void Menu()
    {

        _manager.statusGame = GameManager.GameStatus.RunningGame;
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);

    }

    private void SetText()
    {
        textLife.text = " = " + _manager._indexLife;
    }

    private void TimerLevel()
    {
        timeProgress -= Time.deltaTime;
    }

}
