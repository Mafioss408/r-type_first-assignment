using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundLevel : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private GameObject boss;
    [SerializeField] private AudioClip music;
    [SerializeField] private AudioClip musicBoss;


    private AudioSource musicSource;
    private bool bossSpawn = true;

    private void Awake()
    {
        musicSource = GetComponent<AudioSource>();
    }
    private void Start()
    {
        musicSource.clip = music;
        musicSource.Play();
    }
    void Update()
    {
        if (boss.activeSelf && bossSpawn)
        {
            musicSource.clip = musicBoss;
            musicSource.Play();
            bossSpawn = false;
        }
    }
}
