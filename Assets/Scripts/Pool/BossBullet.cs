using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBullet : MonoBehaviour
{

    private int bulletDamage;
    private float bulletSpeed;

    public int _bulletDamage { get { return bulletDamage; } set { bulletDamage = value; } }
    public float _bulletSpeed { get { return bulletSpeed; } set { bulletSpeed = value; } }

    private void Update()
    {

        moveBullet();
        StartCoroutine(DestroyObj());
    }

    private void moveBullet()
    {
        Vector3 movement = transform.right * (bulletSpeed * Time.deltaTime);
        transform.position += movement;
    }
    private IEnumerator DestroyObj()
    {
        yield return new WaitForSeconds(5f);
        BossPoolManager.Instance.ReturnToPool(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
 
        IDamageable damageable = collision.collider.GetComponent<IDamageable>();
        damageable?.TakeDamage(bulletDamage);

        BossPoolManager.Instance.ReturnToPool(gameObject);
    }


}
