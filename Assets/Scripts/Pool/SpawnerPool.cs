using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerPool : MonoBehaviour
{
    private ProjectilePool pool;
    private EnemyProjectilePool poolEnemy;
    private BossPoolManager poolBoss;

    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform[] muzzle;
    [SerializeField] private int bulletDamage;
    [SerializeField] private int bulletSpeed;

    private void Start()
    {
        pool = ProjectilePool.Instance;
        poolEnemy = EnemyProjectilePool.Instance;
        poolBoss = BossPoolManager.Instance;

    }
    public void Spawn()
    {
        foreach (var item in muzzle)
        {
            GameObject bomb = pool.Get(bullet);
            bomb.transform.position = item.transform.position;
            bomb.gameObject.SetActive(true);

        }
    }
    public void SpawnEnemy()
    {
        foreach (var item in muzzle)
        {
            GameObject bomb = poolEnemy.Get(bullet);
            bomb.GetComponent<EnemyBullet>()._bulletDamage = bulletDamage;
            bomb.GetComponent<EnemyBullet>()._bulletSpeed = bulletSpeed;
            bomb.transform.rotation = item.transform.rotation;
            bomb.transform.position = item.transform.position;
            bomb.gameObject.SetActive(true);

        }
    }

    public void SpawnBoss()
    {
        foreach (var item in muzzle)
        {
            GameObject bomb = poolBoss.Get(bullet);
            bomb.GetComponent<BossBullet>()._bulletDamage = bulletDamage;
            bomb.GetComponent<BossBullet>()._bulletSpeed = bulletSpeed;
            bomb.transform.rotation = item.transform.rotation;
            bomb.transform.position = item.transform.position;
            bomb.gameObject.SetActive(true);

        }
    }
}
