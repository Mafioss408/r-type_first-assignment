using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossPoolManager : MonoBehaviour
{
    private Queue<GameObject> bombs = new Queue<GameObject>();

    public static BossPoolManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private void AddBombs(int count, GameObject bullet)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject projectile = Instantiate(bullet);
            projectile.transform.parent = transform;

            projectile.gameObject.SetActive(false);
            bombs.Enqueue(projectile);
        }
    }

    public GameObject Get(GameObject bullet)
    {
        if (bombs.Count == 0)
        {
            AddBombs(1, bullet);
        }
        return bombs.Dequeue();
    }
    public void ReturnToPool(GameObject bomb)
    {
        bomb.gameObject.SetActive(false);
        bombs.Enqueue(bomb);
    }

}
